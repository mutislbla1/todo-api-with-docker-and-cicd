"use strict";

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    const addTodos = [
      {
        title: "study container",
        isComplete: false,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        title: "study linter",
        isComplete: false,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        title: "study TDD",
        isComplete: false,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        title: "study CI/CD",
        isComplete: false,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        title: "do homework week 11",
        isComplete: false,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ];
    return queryInterface.bulkInsert("Todos", addTodos, {});
  },

  async down(queryInterface, Sequelize) {
    return queryInterface.bulkDelete("Todos", null, {});
  },
};
